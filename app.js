import fetch from 'node-fetch'
import nconf from 'nconf'
import express from 'express'
import humanizeDuration from 'humanize-duration'

nconf.argv().env().file({ file: process.argv[2] || '/home/klipper/printer_data/config/yandex-klipper.json' })
const app = express()

const port = nconf.get('api:port')
const key = nconf.get('api:key')

app.get('/:key/status', async (req, res) => {
    if (req.params.key != key) {
        res.send('Нет доступа!')
    } else {
        const response = await fetch(`http://${nconf.get('klipper:host')}/printer/objects/query?gcode_move&toolhead&webhooks&virtual_sdcard&print_stats&extruder=target,temperature&heater_bed=target,temperature`)
        const data = await response.json()
        let text = ''
        
        if (data.result.status.webhooks.state === 'ready') {
            switch (data.result.status.print_stats.state) {
                case 'standby':
                    text += 'Принтер в режиме ожидания.'
                    break;
                case 'printing':
                    const vsd = data.result.status.virtual_sdcard
                    const pstats = data.result.status.print_stats
                    const total_time = pstats.print_duration / vsd.progress
                    const eta = total_time - pstats.print_duration;
                    text += `Принтер печатает. Печатается ${data.result.status.print_stats.filename}. До конца печати осталось приблизительно ${humanizeDuration(Math.round(eta) * 1000, { language: "ru" } )}.` 
                    break;
                case 'paused':
                    text += `Принтер на паузе. Печатается ${data.result.status.print_stats.filename}. До конца печати осталось приблизительно ${humanizeDuration(Math.round(eta) * 1000, { language: "ru" } )}.`
                    break;
                case 'error':
                    text += `Принтер в ошибке. Описание ошибки: ${data.result.status.print_stats.message}. Печатался ${data.result.status.print_stats.filename}.`
                    break;
                case 'complete':
                    text += `Принтер завершил печать. Печатался ${data.result.status.print_stats.filename}.`
                    break;
                default:
                    break;
            }

            let temperature = ` Температура экструдера - ${data.result.status.extruder.temperature} градусов, а температура стола - ${data.result.status.heater_bed.temperature} градусов.`
            text += temperature
        } else if (data.result.status.webhooks.state === 'shutdown') {
            text += `Принтер не подключён! Сообщение: ${data.result.status.webhooks.state_message}`
        } else {
            text += `Ошибка! Сообщение: ${data.result.status.webhooks.state_message}`
        }

        res.json({"status":"ok","value":`${text}`})
    }
})

app.get('/:key/light', async (req, res) => {
    if (req.params.key != key) {
        res.send('Нет доступа!')
    } else {
        const response = await fetch(`http://${nconf.get('klipper:host')}/printer/gcode/script?script=PRINTER_LIGHT`);
        const data = await response.json();
        if (data.result === 'ok') {
            res.json({"status":"ok","value":`Подсветка принтера переключена!`})
        } else {
            res.json({"status":"ok","value":`Произошла какая-то ошибка!`})
        }
    }
})

app.post('/:key/shutdown', async (req, res) => {
    if (req.params.key != key) {
        res.send('Нет доступа!')
    } else {
        const response = await fetch(`http://${nconf.get('klipper:host')}/machine/shutdown`, {
            method: 'post',
            body: JSON.stringify(req.body),
            headers: {'Content-Type': 'application/json'}
        });
        const data = await response.json();

        if (data.result === 'ok') {
            res.json({"status":"ok","value":`Принтер выключен!`})
        } else {
            res.json({"status":"ok","value":`Произошла какая-то ошибка!`})
        }
    }
})

app.get('*', (req, res) => {
    res.send('Нет доступа!')
})

app.listen(port, () => {
    console.log(`Приложение запущено на порте ${port}.`)
})